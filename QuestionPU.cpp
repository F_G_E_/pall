#include<iostream>
#include<windows.h>
using namespace std;

const int maxSize = 50;
int A[maxSize][maxSize];
int B[maxSize];
int sum[maxSize];
LARGE_INTEGER frequency;

void func2(int n)//循环展开优化算法
{
	for (int j = 0;j < n; j++)
	{
		for (int i = 0; i < n; i+=2){
			sum[i] += A[j][i] * B[j];
            sum[i+1]+=A[j][i+1]*B[j];
        }
	}
}
void func3(int n)//优化算法
{
	for (int j = 0;j < n; j++)
	{
		for (int i = 0; i < n; i++){
			sum[i] += A[j][i] * B[j];
        }
	}
}



int main()
{
	

	//初始化
	for (int i = 0; i < maxSize; i++)
	{
		B[i] = i;
		sum[i] = 0;
		for (int j = 0; j < maxSize; j++)
		{
			A[i][j] = i + j;
		}
	}

    double dff, begin1, end1,time1;
	QueryPerformanceFrequency(&frequency);
	dff = (double)frequency.QuadPart;
	QueryPerformanceCounter(&frequency);
	begin1 = frequency.QuadPart;

    for (int i = 0; i < 1000; i++) {//函数执行100次
        for (int j = 0; j < maxSize; j++) sum[j] = 0;
        func2(maxSize);
    }

	QueryPerformanceCounter(&frequency);
	end1 = frequency.QuadPart;
	time1= (end1 - begin1) / dff;

	cout<<time1<<endl;

    cout<<"finish!"<<endl;


	return 0;
}