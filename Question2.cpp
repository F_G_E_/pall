#include<iostream>
#include<math.h>
#include<windows.h>
using namespace std;


const int maxSize = 524288;//����Ϊ2����������
int a[maxSize];
int sum = 0;
LARGE_INTEGER frequency;


//ƽ���㷨
void func1(int n)
{
	for (int i = 0; i < n; i++) sum += a[i];
}
//����·ʽ
void func2(int n)
{
	int sum1 = 0;
	int sum2 = 0;
	for (int i = 0; i < n; i += 2)
	{
		sum1 += a[i];
		sum2 += a[i + 1];
	}
	sum = sum1 + sum2;
}

//����ѭ��
void func3(int n)
{
	for (int m = n; m > 1; m /= 2)
	{
		for (int i = 0; i < m / 2; i++)
		{
			a[i] = a[i * 2] + a[i * 2 + 1];
		}
	}
	sum = a[0];
}

//�ݹ麯��
void func4(int n)
{
	if (n == 1) return;
	for (int i = 0; i < n / 2; i++)
		a[i] += a[n - i - 1];
	n = n / 2;
	func4(n);
	sum = a[0];
}


int main()
{
	//��ʼ��
	for (int i = 0; i < maxSize; i++) a[i] = i;

	double time1[19];

	for (int countTest = 0; countTest < 19; countTest++)
	{
		int testSize = pow(2, countTest+1);
		time1[countTest] = 0;

		for (int i = 0; i < 10000; i++)
		{
			for (int i = 0; i < maxSize; i++) a[i] = i;
			sum = 0;

			double dff, begin1, end1;
			QueryPerformanceFrequency(&frequency);
			dff = (double)frequency.QuadPart;
			QueryPerformanceCounter(&frequency);
			begin1 = frequency.QuadPart;


			func3(testSize);


			QueryPerformanceCounter(&frequency);
			end1 = frequency.QuadPart;
			time1[countTest] += (end1 - begin1) / dff;
		}

		cout << time1[countTest] << endl;
	}

	system("pause");
	return 0;
}