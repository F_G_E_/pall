#include<iostream>
#include<sys/time.h>
using namespace std;


const int maxSize = 500;
int A[maxSize][maxSize];
int B[maxSize];
int sum[maxSize];

int func1(int n)//优化算法
{
    for(int j=0;j<n;j++)
    for(int i=0;i<n;i++)
    sum[i]+=A[j][i]*B[j];
}

int main()
{
    for(int i=0;i<maxSize;i++)
    {
        B[i] = i;
        sum[i] = 0;
        for(int j=0;j<maxSize;j++) A[i][j] = i+j;
    }
    struct timeval sTime,eTime;
    gettimeofday(&sTime,NULL);
    for(int i=0;i<1000;i++)
    {
        for(int j=0;j<maxSize;j++) sum[j] = 0;
        func1(maxSize);
    }
    gettimeofday(&eTime,NULL);
    float exeTime = (eTime.tv_sec-sTime.tv_sec)*1000000+(eTime.tv_usec-sTime.tv_usec);
    exeTime/=100000;
    cout<<"finish!"<<endl;
    cout<<exeTime<<endl;
}
