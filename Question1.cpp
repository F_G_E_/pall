#include<iostream>
#include<windows.h>
using namespace std;

const int maxSize = 2500;
int A[maxSize][maxSize];
int B[maxSize];
int sum[maxSize];
LARGE_INTEGER frequency;


void func1(int n)//平凡算法
{
	for (int i = 0; i < n; i++) 
	{
		for (int j = 0; j < n; j++)
			sum[i] += A[j][i] * B[j];
	}
}
void func2(int n)//优化算法
{
	for (int j = 0;j < n; j++)
	{
		for (int i = 0; i < n; i++)
			sum[i] += A[j][i] * B[j];
	}
}



int main()
{

	//测试数据规模(26)
	int testSize[] = { 10,30,50,80,100,200,300,400,500,600,700,800,900,
1000,1100,1200,1300,1400,1500,1600,1700,1800,1900,2000,2100,2200 };

	//初始化
	for (int i = 0; i < maxSize; i++)
	{
		B[i] = i;
		sum[i] = 0;
		for (int j = 0; j < maxSize; j++)
		{
			A[i][j] = i + j;
		}
	}

	//算法执行时间
	double time1[26];


	cout << "优化算法：" << endl;
	for (int testCount = 0; testCount < 26; testCount++)//遍历26个测试数据
	{

		double dff, begin1, end1;
		QueryPerformanceFrequency(&frequency);
		dff = (double)frequency.QuadPart;
		QueryPerformanceCounter(&frequency);
		begin1 = frequency.QuadPart;


		for (int i = 0; i < 1000; i++) {//函数执行1000次
			for (int j = 0; j < maxSize; j++) sum[j] = 0;
			func2(testSize[testCount]);
		}

		QueryPerformanceCounter(&frequency);
		end1 = frequency.QuadPart;
		time1[testCount] = (end1 - begin1) / dff;
		cout << time1[testCount] << endl;

	}

	system("pause");
	return 0;
}